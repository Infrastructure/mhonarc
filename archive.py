#!/usr/bin/python2

import os
import os.path
import sys
import gzip
import argparse
import time
import dateutil.parser
import re
import datetime
import email.utils
import tempfile
import shutil
import subprocess
import errno

sys.path.append('/usr/lib/mailman/bin')

try:
        from cStringIO import StringIO
except ImportError:
        from StringIO import StringIO

def human_size(size):
    suffixes = [("",2**10), ("K",2**20), ("M",2**30), ("G",2**40), ("T",2**50)]

    for suf, lim in suffixes:
        if size < lim:
            break

    sizediv = size/float(lim/2**10)
    if suf == "":
        fmt = "%0.0f%s"
    elif sizediv > 100:
        fmt = "%0.0f%s"
    elif sizediv > 10:
        fmt = "%0.1f%s"
    else:
        fmt = "%0.2f%s"

    return fmt % (size/float(lim/2**10), suf)

def get_mailinglist_info(listname):
    """Get the mailing list description from mailman

    The description is in HTML"""
    try:
        import paths
        from Mailman import MailList

        mlist = MailList.MailList(listname, lock=0)
        return mlist.info
    except:
        return ""

class Archiver:
    PUBLIC_ARCHIVE_DIR = "/var/lib/mailman/archives/public"
    PUBLIC_RCFILE = "/home/admin/mhonarc/public-rc/base.rc"
    PRIVATE_ARCHIVE_DIR = "/var/lib/mailman/archives/private"
    PRIVATE_RCFILE = "/home/admin/mhonarc/private-rc/base.rc"

    ERRORLOG = "/var/log/mailman/archive"

    MONTHS = {
        'january':   1,
        'february':  2,
        'march':     3,
        'april':     4,
        'may':       5,
        'june':      6,
        'july':      7,
        'august':    8,
        'september': 9,
        'october':  10,
        'november': 11,
        'december': 12
    }

    _fromlinepattern = (r"From \s*[^\s]+\s+\w\w\w\s+\w\w\w\s+\d?\d\s+"
                        r"\d?\d:\d\d(:\d\d)?(\s+[^\s]+)?\s+\d\d\d\d\s*"
                        r"[^\s]*\s*"
                        "$")

    def __init__(self, listname, private=False, debug=False,
                       start_time = None, end_time = None):

        self.listname = listname
        self.private = private
        self.debug = debug
        self.start_time = start_time
        self.end_time = end_time

        self.re_from = re.compile(self._fromlinepattern)

    def output(self, newmsgs, archivepath):
        """Call mhonarc to archive all messages contained in newmsgs to archivepath"""
        if archivepath is None:
            if self.debug:
                print "ERROR: Cannot archive messages as archivepath has not been defined!"
            return False

        try:
            if self.debug:
                print "Calling mhonarc for %s" % archivepath
            path = os.path.join(self.PRIVATE_ARCHIVE_DIR, self.listname, archivepath)

            # Ensure path actually exists
            if not os.path.exists(path):
                os.makedirs(path, 0755)

            rcfile = self.PRIVATE_RCFILE if self.private else self.PUBLIC_RCFILE

            # Call mhonarc for all messages in 'newmsgs'
            with open(self.ERRORLOG, "ab") as error_fd:
                newmsgs.flush()
                cmd = ['mhonarc', '-umask', '022', '-rcfile', rcfile, '-add', '-outdir', path,
                                  '-definevar', 'ARCHDATE=%s LISTNAME=%s' % (archivepath, self.listname),
                                  '-quiet', newmsgs.name]

                # Already start mhonarc in the background
                p = subprocess.Popen(cmd, stdout=error_fd, stderr=subprocess.STDOUT)

                # Append message(s) to mbox (possibly gzipped)
                if os.path.exists("%s.txt" % path):
                    archivedmsgs = open("%s.txt" % path, 'ab')
                else:
                    archivedmsgs = gzip.GzipFile("%s.txt.gz" % path, 'ab')

                newmsgs.seek(0)
                shutil.copyfileobj(newmsgs, archivedmsgs)
                archivedmsgs.close()

                # Now wait for mhonarc to finish
                p.wait()
        finally:
            # Always ensure newmsgs is cleared
            newmsgs.seek(0)
            newmsgs.truncate(0)

    def determine_time(self, fd):
        """Determines the received time of a message"""
        fd.seek(0)
        msg = email.message_from_file(fd)

        received_texts = msg.get_all('received')
        if received_texts is not None:
            # Determine received time
            for text in received_texts:
                received_time = None
                if ';' not in text:
                    continue

                received_time = email.utils.parsedate_tz(text.rpartition(';')[2])
                if received_time is None:
                    try:
                        received_time = dateutil.parser.parse(text.split(':')[2])
                    except ValueError:
                        continue
                try:
                    received_time = email.utils.mktime_tz(received_time)
                except ValueError:
                    received_time = None

                if received_time is not None:
                    received_time = datetime.datetime.utcfromtimestamp(received_time)
                    break

        else:
            date = msg.get('date')
            received_time = email.utils.parsedate_tz(date)

            if received_time is None:
                try:
                    received_time = dateutil.parser.parse(received_time)
                except:
                    received_time = None

                try:
                    received_time = email.utils.mktime_tz(received_time)
                except:
                    received_time = None

            if received_time is not None:
                received_time = datetime.datetime.utcfromtimestamp(email.utils.mktime_tz(received_time))

        if received_time is None:
            if self.debug:
                print sys.stderr, "Failed to parse time from the received headers!"
                return None

        if self.start_time and received_time < self.start_time:
            return False

        if self.end_time and received_time > self.end_time:
            return False

        # Archive emails per month
        return received_time.strftime("%Y-%B")


    def handle_message(self, fd, newmsgs, archivepath):
        """Process one message

        Called from process_fd"""
        path = self.determine_time(fd)

        if path == False:
            # Message should be ignored
            return archivepath

        # If archivepath changes, send the messages to mhonarc
        if archivepath is not None and path is not None and archivepath != path:
            self.output(newmsgs, archivepath)

        # Add new message to list of messages
        fd.seek(0)
        shutil.copyfileobj(fd, newmsgs)

        return path if path is not None else archivepath

    def process_fd(self, fd):
        """Process a filedescriptor for multiple emails seperated using the mbox format

        Calls handle_message for each individual message"""
        re_from = self.re_from

        blank = True
        archivepath = None
        msg = StringIO()
        # newmsgs contains the emails sent to mhonarc
        with tempfile.NamedTemporaryFile() as newmsgs:
            while 1:
                line = fd.readline()
                if line == "":
                    break

                if line == "\n":
                    blank = True
                elif blank and re_from.match(line):
                    if self.debug:
                        sys.stdout.write(line)

                    if msg.tell():
                        archivepath = self.handle_message(msg, newmsgs, archivepath)

                        # msg has been handled, clear it for the next
                        msg.seek(0)
                        msg.truncate(0)
                else:
                    blank = False

                msg.write(line)

            # End of file: handle existing message, if any
            if msg.tell():
                archivepath = self.handle_message(msg, newmsgs, archivepath)

            # End of file: output existing newmsgs, if any
            if newmsgs.tell():
                self.output(newmsgs, archivepath)

        # If archivepath is set, likely a message has been archived
        # in which case the index has to be updated
        if archivepath:
            Archiver.make_index(self.listname, self.private)


    @classmethod
    def make_index(cls, listname, private=False):
        """Create an index.html page with links to all the individually archived months"""
        path = os.path.join(cls.PRIVATE_ARCHIVE_DIR, listname)

        # If public add symlink from private to public
        if not private and not os.path.lexists(os.path.join(cls.PUBLIC_ARCHIVE_DIR, listname)):
            if not os.path.exists(cls.PUBLIC_ARCHIVE_DIR):
                os.makedirs(cls.PUBLIC_ARCHIVE_DIR, 0755)

            os.symlink(os.path.join(cls.PRIVATE_ARCHIVE_DIR, listname), os.path.join(cls.PUBLIC_ARCHIVE_DIR, listname))

        # Determine the archived directories
        #
        # Archive directory should be: YYYY-MMMMMMM
        # where YYYY=year
        # and   MMMMMMM=month name (January)
        #
        # This function assumes that the months are in English, see
        # Archiver.MONTHS
        dirs = {}
        years = {}
        re_path = re.compile(r'^(\d+)-(\w+)$')
        for a_dir in os.listdir(path):
            mo = re_path.match(a_dir)
            if mo is None or not os.path.isdir(os.path.join(path, a_dir)):
                # Ignore everything which which is not in expected format, or
                # not a directory
                continue

            year = mo.group(1)
            month = mo.group(2).lower()

            # Check if month is in English
            if month in cls.MONTHS:
                monthnr = cls.MONTHS[month]
                dirs[a_dir] = (long(year) * 100) + monthnr
                if not year in years:
                    years[year] = {}
                years[year][monthnr] = (a_dir, month)
            else:
                # Unknown month
                dirs[a_dir] = a_dir

        with open(os.path.join(path, 'index.html'), 'w') as fp:
            # Header
            fp.write('''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link href="/css/layout.css" rel="stylesheet" type="text/css" media="screen">
  <link href="/css/style.css" rel="stylesheet" type="text/css" media="all">
  <link href="/css/archive.css" rel="stylesheet" type="text/css" media="all">
  <link rel="icon" type="image/png" href="https://www.gnome.org/img/logo/foot-16.png">
  <link rel="SHORTCUT ICON" type="image/png" href="https://www.gnome.org/img/logo/foot-16.png">
  <title>The {listname} Archives</title>
</head>

<body>
  <!-- site header -->
  <div id="page_archive">
  
       <div id="global_domain_bar_archive">
        <div class="maxwidth">
          <div class="tab">
            <a class="root" href="https://www.gnome.org/">GNOME.org</a>
          </div>
        </div>
      </div>
      
        
  
        
    <div id="header_archive">
     <div id="logo_bar" class="container_12">
      <div id="logo" class="grid_3">
        <a title="Go to home page" href="https://mail.gnome.org/"><img src="https://static.gnome.org/css/gnome-mail.png" alt="GNOME: Mail Services" /></a>
      </div>

       <div id="top_bar_archive" class="grid_9">
        <div class="left">
          <div class="menu-globalnav-container">
            <ul id="menu-globalnav" class="menu">
              <li id="menu-item-1039" class=
              "menu-item menu-item-type-post_type menu-item-object-page menu-item-1039">
              <a href="https://mail.gnome.org/">Home</a></li>

              <li id="menu-item-1037" class=
              "menu-item menu-item-type-post_type menu-item-object-page menu-item-1037">
              <a href="https://mail.gnome.org/mailman/listinfo/">Mailing Lists</a></li>

              <li id="menu-item-1040" class=
              "menu-item menu-item-type-post_type menu-item-object-page menu-item-1040">
              <a href="https://mail.gnome.org/archives/">List Archives</a></li>
            </ul>
          </div>
        </div>

        <div class="right">
          <form role="search" method="get" id="searchform" action="https://cse.google.com/cse">
            <div>
              <input type="hidden" name="cx" value="013743514357734452612:cqmzna8qgms">
              <label class="hidden">Search</label> <input type="text" name="q" maxlength="255" size="15" class="searchTerms" placeholder="Search" />
            </div>
          </form>
        </div>
      </div>
    </div>
   </div>
 </div> <!-- end of #header -->

<!-- end site header -->

  <div id="body_gnome_archive" class="body">
     <p>
      <a href="https://mail.gnome.org/mailman/listinfo/{listname}">More info on this list...</a>
     </p>'''.format(listname=listname))

            # Reshow general info
            fp.write(get_mailinglist_info(listname))

            # Overview per thread, date, author
            for link in ("thread", "date", "author"):
                fp.write('<h2>By {link_title}<a name="{link}"></a></h2>'.format(link=link, link_title=link.title()))
                fp.write('<table border=0>')
                for year in sorted(years, reverse=True):
                    fp.write('<tr><th>%s</th>' % year)

                    for monthnr in xrange(1, 13):
                        if monthnr in years[year]:
                            fp.write('<td><a href="%s/%s.html">%s</a></td>' % (years[year][monthnr][0], link, years[year][monthnr][1][:3].title()))
                        else:
                            fp.write('<td>&nbsp;</td>')
                    fp.write('</tr>')
                fp.write('</table>')

            # Overview of downloadable versions
            fp.write('<h2>Download<a name="download"></a></h2>')
            fp.write('<table border=0>')
            fp.write('<tr><th>Year</th>')
            for month in sorted(cls.MONTHS, key=cls.MONTHS.get):
                 fp.write('<th>%s</th>' % month[:3].title())

            for year in sorted(years, reverse=True):
                fp.write('<tr><th>%s</th>' % year)
                for monthnr in xrange(1, 13):
                    if monthnr in years[year]:
                        mboxsize = 0
                        a_dir = years[year][monthnr][0]
                        for fmt in ("%s.txt", "%s.txt.gz"):
                            mboxfile = fmt % a_dir
                            if os.path.exists(os.path.join(path, mboxfile)):
                                stat = os.stat(os.path.join(path, mboxfile))
                                mboxsize = stat.st_size
                                break

                        fp.write('<td><a href="%s">%s</a></td>' % (mboxfile, human_size(mboxsize)))
                    else:
                        fp.write('<td>&nbsp;</td>')
                fp.write('</tr>')
            fp.write('</table>')

            # Body
            fp.write('''
  </div> <!-- end of div.body -->

  <div id="footer_community"></div>

  <div id="footer_grass"></div>

  <div id="footer">
    <div class="container_12" id="container_12">
      <div class="links grid_9">
        <div class="menu-footer-container">
          <ul id="menu-footer" class="menu">
            <li id="menu-item-1048" class=
            "menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-1048">
            <a href="/">The GNOME Project</a>

              <ul class="sub-menu">
                <li id="menu-item-1049" class=
                "menu-item menu-item-type-post_type menu-item-object-page menu-item-1049">
                <a href="https://www.gnome.org/about/">About Us</a></li>

                <li id="menu-item-1050" class=
                "menu-item menu-item-type-post_type menu-item-object-page menu-item-1050">
                <a href="https://www.gnome.org/get-involved/">Get Involved</a></li>

                <li id="menu-item-1051" class=
                "menu-item menu-item-type-post_type menu-item-object-page menu-item-1051">
                <a href="https://www.gnome.org/teams/">Teams</a></li>

                <li id="menu-item-1053" class=
                "menu-item menu-item-type-post_type menu-item-object-page menu-item-1053">
                <a href="https://www.gnome.org/support-gnome/">Support GNOME</a></li>

                <li id="menu-item-1054" class=
                "menu-item menu-item-type-post_type menu-item-object-page menu-item-1054">
                <a href="https://www.gnome.org/contact/">Contact Us</a></li>

                <li id="menu-item-2246" class=
                "menu-item menu-item-type-post_type menu-item-object-page menu-item-2246">
                <a href="https://www.gnome.org/foundation/">The GNOME Foundation</a></li>
              </ul>
            </li>

            <li id="menu-item-1047" class=
            "menu-item menu-item-type-custom menu-item-object-custom menu-item-1047">
              <a href="#">Resources</a>

              <ul class="sub-menu">
                <li id="menu-item-1055" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1055">
                <a href="https://developer.gnome.org">Developer Center</a></li>

                <li id="menu-item-1056" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1056">
                <a href="https://help.gnome.org">Documentation</a></li>

                <li id="menu-item-1057" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1057">
                <a href="https://wiki.gnome.org">Wiki</a></li>

                <li id="menu-item-1058" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1058">
                <a href="https://mail.gnome.org/mailman/listinfo">Mailing Lists</a></li>

                <li id="menu-item-1059" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1059">
                <a href="https://wiki.gnome.org/GnomeIrcChannels">IRC Channels</a></li>

                <li id="menu-item-1060" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1060">
                <a href="https://bugzilla.gnome.org/">Bug Tracker</a></li>

                <li id="menu-item-1061" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1061">
                <a href="https://git.gnome.org/browse/">Development Code</a></li>

                <li id="menu-item-1062" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1062">
                <a href="https://wiki.gnome.org/Jhbuild">Build Tool</a></li>
              </ul>
            </li>

            <li id="menu-item-1046" class=
            "menu-item menu-item-type-custom menu-item-object-custom menu-item-1046">
              <a href="/news">News</a>

              <ul class="sub-menu">
                <li id="menu-item-1063" class=
                "menu-item menu-item-type-post_type menu-item-object-page menu-item-1063">
                <a href="https://www.gnome.org/press/">Press Releases</a></li>

                <li id="menu-item-1064" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1064">
                <a href="https://www.gnome.org/start/stable">Latest Release</a></li>

                <li id="menu-item-1065" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1065">
                <a href="https://planet.gnome.org">Planet GNOME</a></li>

                <li id="menu-item-1067" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1067">
                <a href="https://news.gnome.org">Development News</a></li>

                <li id="menu-item-1068" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1068">
                <a href="https://identi.ca/gnome">Identi.ca</a></li>

                <li id="menu-item-1069" class=
                "menu-item menu-item-type-custom menu-item-object-custom menu-item-1069">
                <a href="https://twitter.com/gnome">Twitter</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>

      <div id="footnotes" class="grid_9">
       <p> Copyright &copy; 2005 - <span id="current-year"></span> <a href="https://www.gnome.org/"><strong>The GNOME Project</strong></a>.<br />
       <small><a href="http://validator.w3.org/check/referer">Optimised</a> for <a href=
        "http://www.w3.org/">standards</a>. Hosted by <a href=
        "http://www.redhat.com/">Red Hat</a>.
        Powered by <a href="http://www.list.org/">MailMan</a></small></p>
      </div>
    </div>
  </div>

  </div> <!-- end of div.body -->

  <script type="text/javascript">document.getElementById('current-year').innerHTML = new Date().getFullYear();</script>

</body>
</html>''')


def mkdate(datestr):
    #return time.strptime(datestr, '%Y-%m-%d')
    return dateutil.parser.parse(datestr)

def main():
    description = """Archive an email or an mbox per month using mhonarc"""
    epilog="""Report bugs to https://bugzilla.gnome.org/enter_bug.cgi?product=sysadmin"""
    parser = argparse.ArgumentParser(description=description,epilog=epilog)
    parser.add_argument('--debug',   action='store_true', help='Show debugging messages')
    parser.add_argument('--private', action='store_true', help='Private mailing list')
    parser.add_argument('--start-time', type=mkdate, help='Only archive messages received on or after START_TIME')
    parser.add_argument('--end-time', type=mkdate, help='Only archive messages received before or on END_TIME')
    parser.add_argument('--listname', required=True, help='Name of the mailing list')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--makeindex', action='store_true', help='Recreate the index page to the indivual months')
    group.add_argument('infile', nargs='?', type=argparse.FileType('r'), metavar='FILE',
                       default=sys.stdin, help='email or mbox to archive')


    options = parser.parse_args()

    old_mask = os.umask(0022)

    if options.makeindex:
        Archiver.make_index(options.listname, options.private)
        sys.exit(0)

    archiver = Archiver(options.listname, private=options.private, debug=options.debug,
                        start_time=options.start_time, end_time=options.end_time
    )
    archiver.process_fd(options.infile)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        sys.exit(1)
    except EOFError:
        print('EOF')
        sys.exit(1)
    except IOError, e:
        if e.errno != errno.EPIPE:
            raise
        sys.exit(0)
